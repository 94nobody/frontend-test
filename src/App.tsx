import axios from "axios";
import React, { useState, useEffect } from "react";
import Menu from "./components/Menu/Menu";
import Sidebar from './components/Sidebar/Sidebar';
import Content from './components/Content/Content';
import { Drawer } from 'antd';
import { MenuOutlined } from '@ant-design/icons';
import './App.css';

function App() {
    const viewPort = useViewport();
    const isMobile = viewPort.width <= 600;
    const [sidebarSelected, setSidebarSelected] = useState({
        name: 'Animals',
        category: "animal"
    })
    const [imageSelected, setImageSelected] = useState(null);
    const [isOpen, setIsOpen] = useState<boolean>(false);

    useEffect(() => {
        setIsOpen((false))
    }, [imageSelected])


    return <div className="app">
        {
            isMobile && <div
                onClick={() => { setIsOpen(true) }}
                className="drawerBtn"
            ><MenuOutlined /></div>
        }
        {
            isMobile
                ? <Drawer
                    open={isOpen}
                    placement="left"
                    onClose={() => setIsOpen(false)}
                    width={300}
                    closable={false}
                    bodyStyle={{
                        padding: 0,
                        overflow: "hidden"
                    }}
                >
                    <Menu setSidebarSelected={setSidebarSelected} isMobile={isMobile} setIsOpen={setIsOpen} />
                    <Sidebar sidebarSelected={sidebarSelected} setImageSelected={setImageSelected} />
                </Drawer>
                : <React.Fragment>
                    <Menu setSidebarSelected={setSidebarSelected} />
                    <Sidebar sidebarSelected={sidebarSelected} setImageSelected={setImageSelected} />
                </React.Fragment>
        }
        <Content imageSelected={imageSelected} />
    </div>
}

const useViewport = () => {
    const [width, setWidth] = useState(window.innerWidth);

    useEffect(() => {
        const handleWindowResize = () => setWidth(window.innerWidth);
        window.addEventListener("resize", handleWindowResize);
        return () => window.removeEventListener("resize", handleWindowResize);
    }, []);

    return { width };
};

export default App