import React, { useState } from "react";
import { ReactSVG } from "react-svg";
import styles from './Content.module.css';

function Content({ imageSelected, ...props }) {
    return <div className={styles.content}>
        {
            imageSelected
                ? <div className={styles.contentCheckWrapper}>
                    <img
                        src={imageSelected.metadata[0].value}
                        alt={imageSelected.name}
                        className={styles.contentCheckImage}
                    />
                    <p className={styles.contentCheckText}>{imageSelected.description}</p>
                </div>
                : <div className={styles.contentUncheckWrapper}>
                    <ReactSVG
                        src={"Image.svg"}
                        className={styles.contentUncheckIcon}
                    />
                    <p className={styles.contentUncheckText}>Select the Photo</p>
                </div>
        }

    </div>
}

export default Content;