import React, { useState } from "react";
import styles from './Menu.module.css'
import { ReactSVG } from "react-svg";

function Menu({ setSidebarSelected, ...props }) {
    const [activeKey, setActiveKey] = useState<number>(0);
    const [listMenu, setListMenu] = useState<{
        name: string;
        icon: string;
        category: string
    }[]>([
        { name: "Animals", icon: 'Animal', category: 'animal' },
        { name: "Architecture", icon: 'Architecture', category: "architecture" },
        { name: "Landscapes", icon: 'Image', category: "landscape" },
        { name: "People", icon: 'People', category: "people" }
    ])
    return (
        <div className={styles.menu}>            
            {
                listMenu.map((d, key) => <div
                    key={key}
                    className={styles.menuBox}
                    onClick={() => {
                        setActiveKey(key);
                        setSidebarSelected(d)
                    }}
                >
                    <ReactSVG
                        src={`${d.icon}.svg`}
                        className={`${styles.menuIcon} ${key == activeKey ? styles.menuIconActive : ''}`}
                    />
                    <p className={`${styles.menuTitle} ${key == activeKey ? styles.menuTitleActive : ''}`}>{d.name}</p>
                </div>)
            }
        </div>
    )
}

export default Menu;