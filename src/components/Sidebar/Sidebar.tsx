import React, { useState, useEffect } from "react";
import styles from './Sidebar.module.css';
import { ReactSVG } from "react-svg";
import { Spin } from 'antd';
import axios from "axios";

function Sidebar({ sidebarSelected, setImageSelected, ...props }) {
    const [listImage, setListImage] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        reloadData()
    }, [sidebarSelected.category])

    const reloadData = () => {
        setListImage([]);
        setLoading(true)
        axios.get('https://srv.3dcommerce.studio/public/shop/getProducts?shop=6412c8e785fad7b71567f17b').then((res) => {
            if (res.status == 200) {
                const getData = res.data.data.filter((d) => d.category == sidebarSelected.category);
                // console.log(getData)
                setListImage(getData)
                setLoading(false)
            }
        }).catch((err) => {
            console.log('err', err)
        })
    }

    const [activeKey, setActiveKey] = useState(null);

    return (
        <div className={styles.sidebar}>
            <div className={styles.sidebarHeader}>
                <p className={styles.sidebarTitle}>{sidebarSelected.name}</p>
                <div className={styles.sidebarSearchWrapper}>
                    <ReactSVG
                        src={"Search.svg"}
                        className={styles.sidebarSearchIcon}
                    />
                    <input
                        placeholder="Search for Photo..."
                        className={styles.sidebarSearchInput}
                    />
                </div>
            </div>
            <div className={styles.sidebarContent}>
                <Spin spinning={loading}>
                    <div className={styles.sidebarRow}>
                        {
                            listImage.map((d, key) => <div className={styles.sidebarCol} key={key}>
                                <div
                                    className={`${styles.sidebarImgWrapper} ${activeKey && (activeKey == d.id) ? styles.sidebarImgWrapperActive : ''}`}
                                    onClick={() => {
                                        setActiveKey(d.id);
                                        setImageSelected(d);
                                    }}
                                >
                                    <img
                                        src={d.metadata[0].value}
                                        alt={d.name}
                                        className={styles.sidebarImg}
                                    />
                                </div>
                            </div>)
                        }
                    </div>
                </Spin>
            </div>
        </div>
    )
}

export default Sidebar;